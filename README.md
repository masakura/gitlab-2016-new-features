# GitLab 新機能 2016
今年も GitLab の機能強化の速度は目覚ましい物がありました。一年ずーっとウォッチしてきましたが、GitHub の追いかけではなく、独自の道を歩み始めたような印象を受けました。

それらの機能強化の中から、私が独断で選んだものを簡単に紹介します。

* 注) **EE** と書かれているものは、GitLab Enterprise Edition 用です。GitLab.com では無料で使えますが、オンプレミスで使う場合は有償となります。

https://masakura.gitlab.io/gitlab-2016-new-features

## 変更
* アップデートはその月の 22 日
* ちょっと前から RC 版がリリースされて、GitLab.com に設置される
* GitLab.com である程度テストされてからのリリースなのでちょっと安心
* どの機能がいつ入るかに関しては、[GitLab Direction](https://about.gitlab.com/direction/) で少しだけ分かる


### 2015 年の大きな変更
* GitLab CI の統合
* Mattermost の統合
* Git LFS の対応


### [8.3](https://about.gitlab.com/2015/12/22/gitlab-8-3-released/) 2015/12
* GitLab Pages (EE)
  - CE にも欲しい...
* Issue Weight (EE)
* CI Improvements
  - GitLab CE への CI 取り込みの完了


### [8.4](https://about.gitlab.com/2016/01/22/gitlab-8-4-released/) 2016/01 50's Release!
* CI の成果物ファイルを GitLab から見られるように
* Filter Commit Messages
* Design Updates
  - 某社からクレームが入ったんでしょうかね...


### [8.5](https://about.gitlab.com/2016/02/22/gitlab-8-5-released/) 2016/02
* パフォーマンス向上!


### [8.6](https://about.gitlab.com/2016/03/22/gitlab-8-6-released/) 2016/03
* 公開プロジェクトのプライベート Issue
* Delete Issue!
* Move Issue to other Projects


### [8.7](https://about.gitlab.com/2016/04/22/gitlab-8-7-released/) 2016/04
* Due Date for Issues
* Better Diffs
- 行番号とか + とか - がクリップボードへコピーされなくなった


### [8.8](https://about.gitlab.com/2016/05/22/gitlab-8-8-released/) 2016/05
* Pipelines
* GitLab Container Registry
* Diff view で空白文字の違いを除外できるように
* Milestone references in Markdown


### [8.9](https://about.gitlab.com/2016/06/22/gitlab-8-9-released/) 2016/06
* パフォーマンス!
* File Lock (EE+)
* デプロイ環境を定義できるように
  - この時点では大したことできない
* Priolity Labels
* Custom Notification Level
* Request Access to Project
* CI 成果物の破棄する期限


### [8.10](https://about.gitlab.com/2016/07/22/gitlab-8-10-released/) 2016/07
* パフォーマンス!
* Diff の改善
* CI のジョブを手動でキックできるように
  - ちょっとだけ CD っぽく
* Syntax Higlight の強化
* Slack 連携の強化
* 絵文字!
* インラインビデオ


### [8.11](https://about.gitlab.com/2016/08/22/gitlab-8-11-released/) 2016/08
* Issue Board!
* Branch Permissions for Users (EE)
* Resolve Discussions in MRs
* Issue and MR Templates
* Slash command
* CD で MR!
  - このあたりでちょっと興奮している
* Code Hilight でコードを折りたためるように
* メンバーに有効期限を
* パフォーマンス!


### [GitLab Live Event Recap](https://about.gitlab.com/2016/09/14/gitlab-live-event-recap/)


### [8.12](https://about.gitlab.com/2016/09/22/gitlab-8-12-released/) 2016/09
* Cycle Analytics
* GitLab Code Search (EE)
* Merge Request Versions
* Reveiw Apps (Experimental)
  - かなり興奮している
* Git LFS の SSH 認証対応
* Build permissions changes
  - 昔から使ってる人はちょっと注意
* Submodules in CI


### [8.13](https://about.gitlab.com/2016/10/22/gitlab-8-13-released/) 2016/10
* Multiple Issue Boards (EE)
* Issue Board から Issue を作れるように
* Group Labels


### [8.14](https://about.gitlab.com/2016/11/22/gitlab-8-14-released/) 2016/11
* Time Tracking Beta (EE)
* Chat Commands (experimental)
* Review Apps 完了
