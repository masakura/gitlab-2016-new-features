### [8.5 <small>2016/02</small>](https://about.gitlab.com/2016/02/22/gitlab-8-5-released/)

* [Performance for Scale](https://about.gitlab.com/2016/02/22/gitlab-8-5-released/#performance-for-scale)
* [GitLab Geo Alpha](https://about.gitlab.com/2016/02/22/gitlab-8-5-released/#gitlab-geo-alpha-ee) (EE only)
* [Sort by Votes](https://about.gitlab.com/2016/02/22/gitlab-8-5-released/#sort-by-votes)
